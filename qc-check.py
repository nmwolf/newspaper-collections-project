import sqlite3
import os
import sys
from zipfile import ZipFile
import re

def write_results(newspaper, missed_zips, missed_xmls):
    with open(newspaper + '-QC-report.txt', 'a', encoding='utf-8') as out_file:
        if not len(missed_zips):
            out_file.write("No errors in producing summary counts for " + newspaper + "\n")
        else:
            for zip_file in missed_zips:
                out_file.write(zip_file +  "\n")
        if not len(missed_xmls):
            out_file.write("No missed xml files for " + newspaper + "\n")
        else:
            for i in missed_xmls:
                out_file.write(i[0] + "|" + i[1] + "\n")
    out_file.close()

    
def compile_record_ids(newspaper):
    with open('Records/' + newspaper + '-RecordIDs.txt') as f:
        unsorted_recs = [i.strip('\n') for i in f.readlines()]
        unsorted_recs.sort()
    f.close()
    return unsorted_recs

def compile_zips_complete(newspaper):
    zip_completed_list = []
    with open('count_outputs/' + newspaper + '.txt') as f:
        for line in f.readlines()[1:]:
            zip_completed_list.append(line.split(',')[1])
    return zip_completed_list

def compile_records(newspaper):
    newspaper_contents = {}
    for root, dirs, files in os.walk('newspapers/' + newspaper):
        ziplist = [i for i in files if not i[0] == '.']
        for zip_path in ziplist:
            with ZipFile('newspapers/' + newspaper + '/' + zip_path) as myzip:
                xml_records = [i.replace('.xml','') for i in myzip.namelist()]
                xml_records.sort()
                newspaper_contents[zip_path] = xml_records
    return newspaper_contents

def binary_search(actual_zip, actual_list, parsed_list):
    missed = []
    if len(actual_list) == len(parsed_list):
        return missed
    else:    
        for rec_id in actual_list:
            first = 0
            last = len(parsed_list)-1
            found = False
            while first<=last and not found:
                pos = 0
                midpoint = (first + last)//2
                if parsed_list[midpoint] == rec_id:
                    pos = midpoint
                    found = True
                else:
                    if rec_id < parsed_list[midpoint]:
                        last = midpoint-1
                    else:
                        first = midpoint+1
            if not found:
                missed.append((actual_zip,rec_id))
        return missed
                

def main(newspaper):
    actual_zipfiles = compile_records(newspaper)
    parsed_zipfiles = compile_zips_complete(newspaper)
    parsed_xmls = compile_record_ids(newspaper)
    missed_zips = []
    missed_xmls = []
    for actual_zip in actual_zipfiles:
        if actual_zip in parsed_zipfiles:
            pass
        else:
            missed_zips.append(actual_zip)
        missed_xmls+=binary_search(actual_zip, actual_zipfiles[actual_zip], parsed_xmls)
    write_results(newspaper, missed_zips, missed_xmls)
    
if "__main__" == __name__:
    main(sys.argv[1]) 
