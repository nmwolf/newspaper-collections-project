#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --time=40:00:00
#SBATCH --mem=8GB
#SBATCH --job-name=qc_run-array
#SBATCH --mail-type=END
#SBATCH --mail-user=nmw2@nyu.edu
#SBATCH --output=slurm_qc-run_%j.out
#SBATCH --array=0
  
module purge
module load python3/intel/3.6.3

## First run, full set; array 0-24 
##directories=(AmericanHebrewJewishMessenger AmericanIsraelite AtlantaConstitution AtlantaDailyWorld BostonGlobe ChicagoTribune ChristianScienceMonitor ChineseNewspaperCollection GuardianObserver HartfordCourant IrishTimes JewishAdvocate JewishExponent LosAngelesTimes Newsday NewYorkAmsterdamNews NewYorkTimes NewYorkTribuneHeraldTribune NorfolkJournalGuide PhiladelphiaTribune PittsburghCourier SanFranciscoChronicle TimesOfIndia WallStreetJournal WashingtonPost)

## Second run, recheck of incompletes and two missed in first pass; array=0-4
##directories=(NewYorkTribuneHeraldTribune ChicagoTribune Newsday LosAngelesTimes)

## Third run, for AtlantaConstitution; array=0
directories=(AtlantaConstitution)

dir=${directories[$SLURM_ARRAY_TASK_ID]}

python3 qc-check.py $dir



