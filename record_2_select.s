#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --time=70:00:00
#SBATCH --mem=16GB
#SBATCH --job-name=record_select
#SBATCH --mail-type=END
#SBATCH --mail-user=nmw2@nyu.edu
#SBATCH --output=recordLevel_stats/PhiladelphiaTribune_RecordLevel_Scores.csv

  
module purge
module load sqlite

sqlite3 sqlite/PhiladelphiaTribune.db "select bibliometa.Newspaper, tokenqualityscores.RecordID, tokenqualityscores.NumberTokens, tokenqualityscores.NumberCorrect, tokenqualityscores.ErrorEvaluation, substr(bibliometa.NumericPubDate,1,4) year from tokenqualityscores left join bibliometa on tokenqualityscores.RecordID = bibliometa.RecordID;"
