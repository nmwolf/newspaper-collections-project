import sqlite3
import os
import sys
import xml.etree.cElementTree as et
from zipfile import ZipFile
from multiprocessing import Pool
import re
import time
from collections import Counter
from htrc import htrc_index
from scheduler import schedule

class CorpusCounter(dict):
    def __init__(self, NewspaperName:str, ZipFile:str):
        self.name = NewspaperName
        self.zip = ZipFile
        self.count_dict = {}
        
    def update(self, year, num_tokens, num_correct):
        try:
            self.count_dict[year]['num_correct']+=num_correct
            self.count_dict[year]['num_tokens']+=num_tokens
        except KeyError:
            self.count_dict[year] = {'num_tokens':num_tokens,'num_correct':num_correct}
    

def tokenize_text(ftext):
    """
    Tokenizes a single string full text, returning a list of tokens. First splits on whitespace,
    then removes any trailing non-alpha character (e.g. commas, semicolons, periods), then keeps only tokens
    that have at least one alpha character (e.g. no numbers, punctuation, or pure OCR errors)
    :param ftext: single string consisting of the full text from a newspaper article
    :return: list of tokens meeting paramters outlined above
    
    >>> full_text = "Where is the house? *9^ ca8n't say ., for sure."
    >>> tokenized = tokenize_text(full_text)
    >>> print(tokenized)
    ['Where','is','the','house','ca8n't','say','for','sure']
    """
    return [re.sub(r'[^A-Za-z]$','', i) for i in ftext.split() if re.search(r'[A-Za-z]+', i)]

            
def htcheck(inputstring):
    """
    Performs the checking of a token against the HathiTrust corpus to see if there is a match.
    :param inputstring: a word token (string) to check
    :param htrc_index: the two-character indexed dictionary to check for existence of a token
    :return: Boolean True if the token is in the corpus, False if not
    
    >>> htrc_index = load_ht_corpus()
    >>> print(htcheck('subtle', htrc_index))
    True
    >>> print(htcheck('h&4ks', htrc_index))
    False
    """
    
    try:
        if inputstring in htrc_index[inputstring[0:2]]:
            return True
        else:
            return False
    except:
        return False


def pull_tag(xml_info):
    """
    A function to pull the tag value for a given tag and return a list of all tag values (strings) and the tag/element
    itself as a string. The function also accommodates the special case of Publication tags, which contain
    nested child tags. Otherwise, this function assumes it is working on first-level parent tags and 
    the text they contain.
    It also performs some light cleanup on the Abstract and FullText tag contents. Because there is often a lot of
    extraneous whitespace in these fields, this function removes all trailing and leading whitespace, and then 
    compresses any cases of multiple spaces (\s) or tabs (\t) into a single space chracter.
    :param xml_info: a two element tuple ( to accommodate the map() approach of inser_row() ); the first element
    of the tuple is the tag name, and the second is the XML tree object that will be parsed using XPath
    :return: two-element tuple consisting of a list of tag contents (strings) and the tag (a string) 
    associated with them
    
    """
    elems = []
    if xml_info[0] in ['PublicationID','Title','Qualifier','Edition']:
        path = './/Publication/'
    else:
        path = './/'
    path = path + xml_info[0]
    for elem in xml_info[1].findall(path):  
        if xml_info[0] == 'FullText' or xml_info[0] == 'Abstract':
            compressed_whitespace_text = elem.text.strip()
            compressed_whitespace_text = re.sub(r'\s{2,}|\t+', ' ', compressed_whitespace_text)
            elems.append(compressed_whitespace_text)
        else:
            elems.append(elem.text)
    return (elems, xml_info[0])
 
    
def write_corpus_meta(counter):
    file_path = 'count_outputs/' + counter.name + '.txt'
    os.makedirs(os.path.dirname(file_path), exist_ok=True)
    with open(file_path, 'a', encoding='utf-8') as out_file:
        for yr_info in counter.count_dict:
            out_file.write(",".join([counter.name, counter.zip, yr_info, str(counter.count_dict[yr_info]['num_tokens']), str(counter.count_dict[yr_info]['num_correct'])]) +  "\n")
    out_file.close()

    
    
def insert_row(row_dict, product_dict, contrib_dict, token_features_list, token_score, newspaper, zipname, filename):
    """
    A function to handle adding all of the parsed XML metadata and full text to the relevant SQLite tables. It also
    has exception handling for database lock and any non-unique DB integrity errors; in such cases, it will wait
    a few seconds and then try again. After 10 attempts, it will ask the user to allow it to continue.
    :param row_dict: dictionary containg the main metadata, with the tag as key and lists of tag contents as values
    :param product_dict: a separate dictionary to handle the multi-valued Product tag
    :param contrib_dict: a separate dictionary to handle the multi-valued Contributor tag
    :param token_features_list: a list of four-element tuples containing the RecordID, token, token's position order
    full text, and a 0/1 designation for whether it was an OCR error, i.e. 0 for no error, 1 for error
    :param token_score: four-element tuple consisting of the RecordID, number of tokens in the XML document,
    number of tokens deemed correct, and the number of tokens correct divided by total tokens
    :param newspaper: a string referring to the name of the newspaper being parsed (same as parent directory to zip files)
    :param zipname: the filename of the zipfile being processed
    :param filename: the filename of the XML file being processed
    
    """
    conn = sqlite3.connect('sqlite/' + newspaper + '.db', timeout=10)
    cursor = conn.cursor()
    attempts = 0
    while attempts < 11:
        try:
            cursor.execute("INSERT INTO bibliometa VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
                          (row_dict['Version'],row_dict['RecordID'],row_dict['DateTimeStamp'],
                           row_dict['ActionCode'],row_dict['RecordTitle'],row_dict['PublicationID'],
                          row_dict['Title'], row_dict['Qualifier'], row_dict['Edition'],
                          row_dict['Pagination'],row_dict['AlphaPubDate'],row_dict['NumericPubDate'],
                          row_dict['SourceType'],row_dict['LanguageCode'],row_dict['Abstract'],
                          row_dict['StartPage'],row_dict['URLDocView'],newspaper, zipname, filename))
            for k,v in product_dict.items():
                cursor.execute("INSERT INTO productids VALUES (?,?,?)",(row_dict['RecordID'],k,v))
            if isinstance(row_dict['ObjectType'], str):
                cursor.execute("INSERT INTO objecttypes VALUES (?,?)",(row_dict['RecordID'],row_dict['ObjectType']))
            else:
                for val in row_dict['ObjectType']:
                    cursor.execute("INSERT INTO objecttypes VALUES (?,?)",(row_dict['RecordID'],val))
            for instance_num,vals in contrib_dict.items():
                cursor.execute("INSERT INTO contributors VALUES (?,?,?,?,?,?,?,?,?,?,?)",
                                   (row_dict['RecordID'], instance_num, vals['ContribRole'],
                                   vals['LastName'], vals['MiddleName'], vals['FirstName'],
                                   vals['PersonName'], vals['OriginalForm'], vals['OrganizationName'],
                                   vals['NameSuffix'], vals['PersonTitle']))
            if token_features_list is not None:
                for token_tuple in token_features_list:
                    cursor.execute("INSERT INTO tokenqualitycheck VALUES (?,?,?,?)", token_tuple)
                cursor.execute("INSERT INTO tokenqualityscores VALUES (?,?,?,?)", token_score)
            conn.commit()
            attempts = 11
        except sqlite3.IntegrityError as e:
            attempts = 11
        except sqlite3.OperationalError as e:
            time.sleep(3)
            attempts+=1
            if attempts > 10:
                print("More than 10 OperationError occurrences (newspaper db). Failure to write row to db: {} | {} | {}".format(newspaper, zipname, filename))
                attempts = 11
    conn.close()
    
    

def parse_xml(input_zip_directory):
    """
    This function does the main work of extracting a zip file, then opening each contained XML file, loading
    the XML into an XML etree object, and calling the relevant functions to parse known tags, build dictionaries
    and lists, tokenize the full text, check each token for OCR quality, record error results, count tokens, and
    assign to each token a position order within the full text. Finally, it manages calling the function to 
    insert the collected data into the SQLite DB.
    
    """
    zip_article, direc = input_zip_directory[0], input_zip_directory[1]
    print("Parsing file ", direc + '/' + zip_article)
    meta_counter = CorpusCounter(direc, zip_article)
    
    with ZipFile('newspapers/' + direc + '/' + zip_article) as myzip:
        for xml_file in myzip.namelist():
            with myzip.open(xml_file, 'r') as f:
                fulltree = f.read()
                xml_tree = et.fromstring(fulltree)
                tags_list = ['Version','RecordID','DateTimeStamp','ActionCode','RecordTitle',
                             'PublicationID','Title','Qualifier','Edition','Pagination',
                             'Publisher','AlphaPubDate','NumericPubDate','SourceType','ObjectType',
                             'LanguageCode','StartPage','URLDocView','Abstract',
                             'FullText']
                main_dict = {}
                for val in map(pull_tag, [(i, xml_tree) for i in tags_list]):
                    main_dict[val[1]] = val[0][0] if len(val[0]) == 1 else val[0] if len(val[0]) != 0 else "NONE"
                    if len(val[0]) > 1 and val[1] != 'ObjectType':
                        print("Warning: multivalue tag unexpectedly found: ", val[1], '|', val[0], '|', direc, '|', xml_file)
                
                # Now we need additional tables for multi-value fields: Product and Contributor
                # Product consists of one or more ProductIDs and accompanying info about full text availability
                # ProductIDs are nonunique across multiple xmls but are treated here as unique within the xml record
                # Contributor consist of any number of contributor name, role, organization name, etc. child tags
                
                productid_dict = {}
                for elem in xml_tree.findall('.//Product/*'):
                    if elem.tag == 'ProductID':
                        product_id = elem.text 
                    elif elem.tag == 'HasFullText':
                        full_text_true = elem.text
                    try:
                        productid_dict[product_id] = full_text_true
                    except UnboundLocalError:
                        productid_dict[product_id] = None
                
                contrib_dict = {}
                contrib_count = 100001
                for contrib_group in xml_tree.findall('Contributor'):
                    contrib_dict[str(contrib_count)] = {'ContribRole':'NONE','LastName':'NONE',
                                                        'MiddleName':'NONE','FirstName':'NONE',
                                                        'PersonName':'NONE','OriginalForm':'NONE',
                                                        'OrganizationName':'NONE','NameSuffix':'NONE',
                                                        'PersonTitle':'NONE'}
                    for elem in contrib_group:
                        contrib_dict[str(contrib_count)][elem.tag] = elem.text
                    contrib_count+=1
                    
                # Tokenizing, OCR accuracy check, and token position identification for English-language texts
                
                if main_dict['LanguageCode'].lower() == 'eng':
                    token_features_list = []
                    num_correct = 0
                    token_list = tokenize_text(main_dict['FullText'])
                    total_tokens = len(token_list)
                    for token,freq in Counter(token_list).items():
                        if htcheck(token.lower()):
                            num_correct+=freq
                            token_features_list.append((main_dict['RecordID'],token, freq, 0))
                        else:
                            token_features_list.append((main_dict['RecordID'],token, freq, 1))
                    try:
                        perc_correct = num_correct/total_tokens
                    except ZeroDivisionError:
                        perc_correct = None
                        
                    token_score = (main_dict['RecordID'], total_tokens, num_correct, perc_correct)
                    
                else:
                    token_features_list = None
                    token_score = None
                
                
                # Update our overall tracking
                
                meta_counter.update(main_dict['NumericPubDate'][0:4],total_tokens, num_correct)
                    
                
                # Lastly, the collected parsed information is sent to be inserted into the SQLite DB
                
                insert_row(main_dict, productid_dict, contrib_dict, token_features_list, token_score,
                           direc, zip_article, str(xml_file))
            f.close()
    
    # Write final summary of zip file's tokens to database:
    
    write_corpus_meta(meta_counter)
    

def main(arg_in):
    start = time.time()
    newspaper_direc, iter = arg_in.split('_')
    if arg_in in schedule:
        filelist = [(i, newspaper_direc) for i in schedule[arg_in]]
    else:
        for root, dirs, files in os.walk('newspapers/' + newspaper_direc):
            filelist = [(i, newspaper_direc) for i in files if not i[0] == '.']
    for zip_article in filelist:
        parse_xml(zip_article)
    end = time.time()
    print("Time elapsed = ", (end-start)/3600, "hours. Parsing of ", arg_in, " is complete.")

    
if "__main__" == __name__:
    main(sys.argv[1])    
