import sqlite3
import sys
import time


"""
Separate script to build all tables ahead of main script

"""

def corpus_meta_db():
    conn = sqlite3.connect('sqlite/corpus.db', timeout=10)
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS corpusmeta
                      (Newspaper text not null, Zipfile text not null, Year text not null, 
                      NumberTokens numeric not null, NumberCorrect numeric not null) 
                   """)
    conn.commit()
    conn.close()


def build_sql_tables(db_name):
    conn = sqlite3.connect('sqlite/' + db_name + '.db', timeout=10)
    cursor = conn.cursor()
    cursor.execute("""CREATE TABLE IF NOT EXISTS bibliometa
                      (Version text not null, RecordID text not null unique, DateTimeStamp text not null, 
                       ActionCode text not null, RecordTitle text not null, PublicationID text not null,
                       Title text not null, Qualifier text not null, Edition text not null, Pagination text not null, 
                       AlphaPubDate text not null, NumericPubDate text not null,SourceType text not null,
                       LanguageCode text not null, Abstract text not null, StartPage text not null,
                       URLDocView text not null, Newspaper text not null, ZipFile text not null,
                       Filename text not null) 
                   """)
    cursor.execute("""CREATE TABLE IF NOT EXISTS productids
                      (RecordID text not null, ProductID text not null, TextAvail text not null) 
                   """)

    cursor.execute("""CREATE TABLE IF NOT EXISTS objecttypes
                      (RecordID text not null, ObjectType text not null) 
                   """)

    cursor.execute("""CREATE TABLE IF NOT EXISTS contributors
                      (RecordID text not null, LocalInstanceNum text not null,
                       ContribRole text, LastName text, MiddleName text, FirstName text,
                       PersonName text, OriginalForm text, OrganizationName text, NameSuffix text,
                       PersonTitle text) 
                   """)

    cursor.execute("""CREATE TABLE IF NOT EXISTS tokenqualitycheck
                      (RecordID text not null, Token text not null, TokenFrequency numeric not null,
                      ErrorEvaluation numeric not null) 
                   """)

    cursor.execute("""CREATE TABLE IF NOT EXISTS tokenqualityscores
                      (RecordID text not null, NumberTokens numeric not null, NumberCorrect numeric not null,
                      ErrorEvaluation numeric) 
                   """)

    conn.commit()
    conn.close()



def main(arg_in, build_type):
    start = time.time()
    newspaper_direc, iter = arg_in.split('_')

    if build_type == 'main':
        corpus_meta_db()
    else:
        build_sql_tables(newspaper_direc)
    
    end = time.time()
    print("Table build complete. Time elapsed = ", (end-start)/3600, "hours.")

    
if "__main__" == __name__:
    main(sys.argv[1], sys.argv[2])
