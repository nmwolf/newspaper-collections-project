#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --time=70:00:00
#SBATCH --mem=8GB
#SBATCH --job-name=pq_parse-array
#SBATCH --mail-type=END
#SBATCH --mail-user=nmw2@nyu.edu
#SBATCH --output=slurm_pq-parse_%j.out
#SBATCH --array=0-2
  
module purge
module load python3/intel/3.6.3

## Iteration 1; array=0-11
## directories=(BaltimoreAfroAmerican_0 BostonGlobe_0 ChicagoTribune_0 ChineseNewspaperCollection_0 ChristianScienceMonitor_0 GuardianObserver_0 HartfordCourant_0 IrishTimes_0 JewishAdvocate_0 JewishExponent_0 LosAngelesTimes_0 Newsday_0)

## Iteration 2; array=0-10
## directories=(BostonGlobe_1 ChicagoTribune_1 ChristianScienceMonitor_1 GuardianObserver_1 HartfordCourant_1 IrishTimes_1 LosAngelesTimes_0 Newsday_0 NewYorkTimes_0 NorfolkJournalGuide_0 PhiladelphiaTribune_0)

## Iteration 3; array=0-9
##directories=(BostonGlobe_2 ChristianScienceMonitor_2 HartfordCourant_2 IrishTimes_2 LosAngelesTimes_1 Newsday_1 NewYorkAmsterdamNews_0 NewYorkTimes_1 NewYorkTribuneHeraldTribune_0 TimesOfIndia_0)

## Iteration 4; array=0-9
##directories=(BostonGlobe_3 ChristianScienceMonitor_3 IrishTimes_3 LosAngelesTimes_2 Newsday_2 NewYorkTimes_2 NewYorkTribuneHeraldTribune_1 SanFranciscoChronicle_0 TimesOfIndia_1 WashingtonPost_0)

## Iteration 5; array=0-7
##directories=(BostonGlobe_4 NewYorkTimes_3 NewYorkTribuneHeraldTribune_3 PittsburghCourier_0 SanFranciscoChronicle_0 TimesOfIndia_1 WallStreetJournal_0 WashingtonPost_0)

## Iteration 6; array=0-6
##directories=(BostonGlobe_5 NewYorkTribuneHeraldTribune_4 SanFranciscoChronicle_1 TimesOfIndia_2 WallStreetJournal_1 WashingtonPost_1 NewYorkTimes_4)

## Iteration 7; array=0-1
##directories=(TimesOfIndia_3 WashingtonPost_2)

## Iteration 8 (finish partial runs); array=0-2
directories=(ChicagoTribune_99 NewYorkTribuneHeraldTribune_99 AtlantaConstitution_99)

dir=${directories[$SLURM_ARRAY_TASK_ID]}

python3 table_builder.py $dir none

python3 xml_sql_parse_single.py $dir



